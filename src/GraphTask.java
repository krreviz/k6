import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    public void run() {

        Function<Vertex, String> printNonBridges = vertex -> "non bridge arc(s): " + vertex.findNonBridgeArcs();
        Function<Vertex, String> printBridges = vertex -> "bridge arc(s): " + vertex.findBridgeArcs();

        Function<Vertex, String> printUsedVertices = vertex -> vertex.visited ? "was used" : "not used";
        Function<Vertex, String> printPathsInTree = vertex -> {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            Arc arc = vertex.first;
            while (arc != null) {
                sb.append(arc).append(" -> ").append(arc.inTree ? "in tree" : "not in tree");
                if (arc.next != null) sb.append(",").append(" ");
                arc = arc.next;
            }
            sb.append("]");
            return sb.toString();
        };

        Function<Integer[], List<Vertex>> v;
        List<Vertex> vertices;

        HashMap<Vertex, List<Vertex>> graphMap = new HashMap<>();
        vertices = createVertices(7);
        v = getVerticesByNumberGenerator(vertices);
        graphMap.put(vertices.get(1), v.apply(i(2, 3)));
        graphMap.put(vertices.get(2), v.apply(i(1, 3)));
        graphMap.put(vertices.get(3), v.apply(i(1, 2, 4)));
        graphMap.put(vertices.get(4), v.apply(i(3, 5)));
        graphMap.put(vertices.get(5), v.apply(i(4, 6, 7)));
        graphMap.put(vertices.get(6), v.apply(i(5, 7)));
        graphMap.put(vertices.get(7), v.apply(i(5, 6)));


        HashMap<Vertex, List<Vertex>> graphMap1 = new HashMap<>();
        vertices = createVertices(8);
        v = getVerticesByNumberGenerator(vertices);
        graphMap1.put(vertices.get(1), v.apply(i(2, 3)));
        graphMap1.put(vertices.get(2), v.apply(i(1, 3, 4)));
        graphMap1.put(vertices.get(3), v.apply(i(1, 2, 5)));
        graphMap1.put(vertices.get(4), v.apply(i(2, 5, 6)));
        graphMap1.put(vertices.get(5), v.apply(i(3, 4)));
        graphMap1.put(vertices.get(6), v.apply(i(4, 7, 8)));
        graphMap1.put(vertices.get(7), v.apply(i(6, 8)));
        graphMap1.put(vertices.get(8), v.apply(i(6, 7)));


        HashMap<Vertex, List<Vertex>> graphMap2 = new HashMap<>();
        vertices = createVertices(12);
        v = getVerticesByNumberGenerator(vertices);
        graphMap2.put(vertices.get(1), v.apply(i(2, 3)));
        graphMap2.put(vertices.get(2), v.apply(i(1, 4)));
        graphMap2.put(vertices.get(3), v.apply(i(1, 4, 8, 9)));
        graphMap2.put(vertices.get(4), v.apply(i(2, 3, 5, 7)));
        graphMap2.put(vertices.get(5), v.apply(i(4, 6)));
        graphMap2.put(vertices.get(6), v.apply(i(5, 7, 10, 11, 12)));
        graphMap2.put(vertices.get(7), v.apply(i(4, 6)));
        graphMap2.put(vertices.get(8), v.apply(i(3)));
        graphMap2.put(vertices.get(9), v.apply(i(3)));
        graphMap2.put(vertices.get(10), v.apply(i(6)));
        graphMap2.put(vertices.get(11), v.apply(i(6)));
        graphMap2.put(vertices.get(12), v.apply(i(6)));




        HashMap<Vertex, List<Vertex>> graphMap3 = new HashMap<>();
        vertices = createVertices(7);
        v = getVerticesByNumberGenerator(vertices);
        graphMap3.put(vertices.get(1), v.apply(i(2, 4)));
        graphMap3.put(vertices.get(2), v.apply(i(1, 3, 4)));
        graphMap3.put(vertices.get(3), v.apply(i(2, 4, 5, 6)));
        graphMap3.put(vertices.get(4), v.apply(i(1, 2, 3, 5, 6)));
        graphMap3.put(vertices.get(5), v.apply(i(3, 4, 6)));
        graphMap3.put(vertices.get(6), v.apply(i(3, 4, 5, 7)));
        graphMap3.put(vertices.get(7), v.apply(i(6)));




        // TODO!!! Your experiments here
        long nanoTime;

        Graph g = new Graph("G");
        g.createRandomSimpleGraph(2000, 6000);
        nanoTime = timeIt(g::markAllBridgesTree, g.first);
        System.out.println("Elapsed time in milliseconds: " + nanoTime / 1e6 + "ms");


        Graph linearGraph = new Graph("L");
        linearGraph.createLinearGraph(2500);
        nanoTime = timeIt(linearGraph::markAllBridgesTree, linearGraph.first);
        System.out.println("Elapsed time in milliseconds: " + nanoTime / 1e6 + "ms");


        Graph sevenEight = createGraphFromMap(graphMap, "sevenEight");
        System.out.println(sevenEight);
        sevenEight.findTree(sevenEight.first);
        sevenEight.markAllBridgesTree(sevenEight.first);
        sevenEight.printAllVerticesParameter(printBridges);
        System.out.println();
        System.out.println();
        System.out.println();


        Graph first = createGraphFromMap(graphMap1, "first");
        System.out.println(first);
        first.markAllBridgesTree(first.first);
        first.printAllVerticesParameter(printBridges);
        System.out.println();
        System.out.println();
        System.out.println();

        Graph second = createGraphFromMap(graphMap2, "second");
        System.out.println(second);
        second.markAllBridgesTree(second.first);
        second.printAllVerticesParameter(printBridges);
        System.out.println();
        System.out.println();
        System.out.println();

        Graph third = createGraphFromMap(graphMap3, "third");
        System.out.println(third);

        third.findTree(third.first);
        third.printAllVerticesParameter(printPathsInTree);
        third.markAllBridgesTree(third.first);
        third.printAllVerticesParameter(printBridges);
        System.out.println();
        System.out.println();
        System.out.println();
    }

    private Function<Integer[], List<Vertex>> getVerticesByNumberGenerator(List<Vertex> vertices) {
        return (Integer... numbers) -> Arrays.stream(numbers)
                .map(vertices::get)
                .collect(Collectors.toList());
    }

    private long timeIt(Consumer<Vertex> workCode, Vertex vertex) {
        long startTime = System.nanoTime();
        workCode.accept(vertex);
        long endTime = System.nanoTime();
        return endTime - startTime;
    }

    private Integer[] i(Integer... numbers) {
        return numbers;
    }

    private Graph createGraphFromMap(HashMap<Vertex, List<Vertex>> graphMap, String name) {
        Graph result = new Graph(name, null);
        List<Vertex> vertices = graphMap.keySet()
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparing(vertex -> Integer.parseInt(vertex.id.substring(1)))))
                .collect(Collectors.toList());

        for (Vertex vertex : vertices) {
            vertex.next = result.first;
            result.first = vertex;
            for (Vertex target : graphMap.get(vertex)) {
                result.createArc("a" + vertex.toString() + "_" + target.toString(), vertex, target);
            }
        }
        return result;
    }

    private List<Vertex> createVertices(int count) {

        return IntStream.range(0, count + 1)
                .mapToObj(value -> new Vertex("v" + value))
                .collect(Collectors.toList());
    }

    /**
     * Ülesandeks on leida sidusa lihtgraafis kõik sillad.
     * Kasutan sügavuti otsingu meetodi,et leida aluspuu ja märgida kõik sillad.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        // You can add more fields, if needed
        public boolean visited = false;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        // TODO!!! Your Vertex methods here!

        /**
         *
         * @param target
         * @return
         *
         */
        public Arc getArcToTarget(Vertex target) {
            Arc arc = first;
            while (arc != null) {
                if (arc.target.equals(target)) return arc;
                arc = arc.next;
            }
            throw new RuntimeException(String
                    .format("There is no arc from %s to %s", this, target));
        }

        /**
         * Meetod, mis määrab mõlemas suunas antud serv sillaks.
         *
         * @param arc
         */
        public void markAsBridge(Arc arc) {
            if (notBelong(arc)) throw new RuntimeException(String
                    .format("Vertex %s is not source for arc %s", this, arc));

            arc.isBridge = true;
            Vertex target = arc.target;
            target.getArcToTarget(this).isBridge = true;
        }

        /**
         * Määrab kasutatuks antud serv mõlemas suunas.
         * @param arc
         */
        public void markAsUsed(Arc arc) {
            if (notBelong(arc)) throw new RuntimeException(String
                    .format("Vertex %s is not source for arc %s", this, arc));

            arc.inTree = true;
            Vertex target = arc.target;
            target.getArcToTarget(this).inTree = true;
        }

        /**
         * Tagastab true, kui antud serv ei kuulu Vertex.
         * @param arc
         * @return
         */
        private boolean notBelong(Arc arc) {
            Arc start = first;
            while (start != null) {
                if (start.equals(arc)) return false;
                start = start.next;
            }
            return true;
        }

        /**
         * Tagastab listi servadest, mis ei kuulu sillade hulka.
         * @return
         */
        public List<Arc> findNonBridgeArcs() {
            ArrayList<Arc> result = new ArrayList<>();
            Arc arc = first;
            while (arc != null) {
                if (!arc.isBridge) result.add(arc);
                arc = arc.next;
            }
            return result;
        }

        /**
         * Tagastab listi servadest, mis kuuluvad sillade hulka.
         * @return
         */
        public List<Arc> findBridgeArcs() {
            ArrayList<Arc> result = new ArrayList<>();
            Arc arc = first;
            while (arc != null) {
                if (arc.isBridge) result.add(arc);
                arc = arc.next;
            }
            return result;
        }

    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;
        // You can add more fields, if needed
        public boolean isBridge = false;
        public boolean inTree = false;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

        // TODO!!! Your Arc methods here!

    }


    class Graph {

        private String id;
        private Vertex first;
        private int info = 0;
        // You can add more fields, if needed

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + String.valueOf(n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        // TODO!!! Your Graph methods here! Probably your solution belongs here.

        /**
         *
         * Meetod otsib aluspuu, liigub punktist punkti.
         * Võimatu läbi käia punkti ning serva mitu korda.
         * Külastatud punkt ning serv märgistatakse kasutatuks.
         * @param vertex
         */
        public void findTree(Vertex vertex) {
            vertex.visited = true;
            Arc arc = vertex.first;
            while (arc != null) {
                Vertex target = arc.target;
                if (target.visited) {
                    arc = arc.next;
                    continue;
                }
                vertex.markAsUsed(arc);
                findTree(target);
                arc = arc.next;
            }
        }

        /**
         * Meetod, mis märgistab kõik graafi sillad.
         * @param root
         */
        public void markAllBridgesTree(Vertex root) {
            findTree(root);
            findNearestVertexToRootYouCanReachUsingBacktracs(root, new ArrayList<>());
        }

        /**
         *Kasutades backtrack tagastab puujuuri lähima Vertex, mille saab saavutada.
         *
         * @param vertex        tipp, millal kontrollitakse backtrack
         * @param partialPath   läbi käidud tee Vertex saavutamiseks
         * @return
         *
         */

        public Vertex findNearestVertexToRootYouCanReachUsingBacktracs(Vertex vertex, List<Vertex> partialPath) {
            Arc arc = vertex.first;
            List<Vertex> tmpPath = new ArrayList<>(partialPath);
            tmpPath.add(vertex);

            Vertex result = vertex;
            while (arc != null) {
                Vertex target = arc.target;

                if (!arc.inTree && tmpPath.contains(target))
                    result = tmpPath.indexOf(target) < tmpPath.indexOf(result) ? target : result;


                if (arc.inTree && !tmpPath.contains(target)) {

                    Vertex childResult = findNearestVertexToRootYouCanReachUsingBacktracs(target, tmpPath);

                    if (!tmpPath.contains(childResult))
                        vertex.markAsBridge(arc);

                    else if (tmpPath.indexOf(childResult) < tmpPath.indexOf(result))
                        result = childResult;
                }


                arc = arc.next;
            }

            return result;
        }

        /**
         * Meetod, mis loob lineaarse graafi, näiteks: 1 - 2 - 3 - (n-1) - n
         *
         * @param n
         */
        public void createLinearGraph(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    String aid = "a" + varray[i].toString() + "_" + varray[i - 1].toString();
                    createArc(aid, varray[i], varray[i - 1]);
                    createArc(aid, varray[i - 1], varray[i]);
                }
            }
        }


        private void printAllVerticesParameter(Function<Vertex, String> function) {
            System.out.println();
            Vertex vertex = first;
            while (vertex != null) {
                System.out.printf("Vertex %s: %s\n", vertex, function.apply(vertex));
                vertex = vertex.next;
            }
        }
    }

}